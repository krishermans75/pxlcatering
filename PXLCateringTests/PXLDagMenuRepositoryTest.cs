﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PXLCateringPCL.Repository;
using PXLCateringPCL.Model;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace PXLCateringTests
{
    [TestClass]
    public class PXLDagMenuRepositoryTest
    {
        private IPXLDagMenuRepository repo;
        
        [TestInitialize]
        public void SetUp()
        {
            repo = new PXLDagMenuRepository();
        }
        
        [TestMethod]
        public void TestHuidigWeekMenu()
        { // Test method cannot be marked async, even if you call async methods
          // Use Result property to making blocking calls

            ObservableCollection<DagMenu> weekmenu = repo.HuidigWeekMenu().Result;
            Assert.IsNotNull(weekmenu);
            Assert.AreEqual(5, weekmenu.Count);
        }

        [TestMethod]
        public void TestMaandagVanHuidigeWeek()
        {
            DateTime day;
            Assert.IsFalse(DateTime.TryParse(repo.MaandagHuidigeWeek(), out day));
            ObservableCollection<DagMenu> weekmenu = repo.HuidigWeekMenu().Result;
            Assert.IsTrue(DateTime.TryParse(repo.MaandagHuidigeWeek(), out day));
            Assert.AreEqual(DayOfWeek.Monday, day.DayOfWeek);
        }

        [TestMethod]
        public void TestHuidigDagMenu()
        {
            DagMenu dm = repo.HuidigDagMenu().Result;
            Assert.IsNotNull(dm);
        }
    }
}
