﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PXLCateringPCL.Util;

namespace PXLCateringTests
{
    [TestClass]
    public class MarkupTest
    {   
        [TestMethod]
        public void TestRemove()
        {
            Assert.AreEqual("Venkelsoep",
                            Markup.Remove(@"<p>Venkelsoep</p>"));
            Assert.AreEqual("Venkelsoep",
                            Markup.Remove("Venkelsoep"));
            Assert.AreEqual("Hongaarse goulash\\r\\nSaladbar",
                Markup.Remove("<p>Hongaarse goulash</p>\\r\\n<p>Saladbar</p>"));
            Assert.AreEqual("1 mei feestdag van de arbeid",
                Markup.Remove("<p style=\\\"MARGIN: 0cm 0cm 0pt\\\" class=\\\"MsoNormal\\\">1 mei feestdag van de arbeid</p>"));
        }
    }
}
