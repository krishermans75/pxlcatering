﻿using PXLCateringApp.Common;
using PXLCateringPCL.Model;
using PXLCateringPCL.Repository;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PXLCateringApp.ViewModel
{
    public class WeekMenuViewModel : BindableBase
    {
        private IPXLDagMenuRepository repo;
        private ObservableCollection<DagMenu> huidigWeekMenu;
        private string weekTitle;

        public WeekMenuViewModel()
        {
            repo = new PXLDagMenuRepository();
            LoadMenuCommand = new RelayCommand(LoadWeekMenu);
            weekTitle = "";
        }

        public string WeekTitle
        {
            get
            {
                if (weekTitle.Equals("")) return "";
                return "Week van " + weekTitle;
            }
            set
            {
                SetProperty<string>(ref weekTitle, value);
            }
        }

        public ObservableCollection<DagMenu> HuidigWeekMenu
        {
            get
            {
                return huidigWeekMenu;
            }

            set
            {
                if (value != huidigWeekMenu)
                {
                    huidigWeekMenu = value;
                    OnPropertyChanged("HuidigWeekMenu");
                }
            }
        }

        public RelayCommand LoadMenuCommand { get; set; }     
        
        private async void LoadWeekMenu()
        {
            // wordt opgeroepen via Behavior tijdens het laden
            HuidigWeekMenu = await repo.HuidigWeekMenu();
            WeekTitle = repo.MaandagHuidigeWeek();
        }
    }
}
