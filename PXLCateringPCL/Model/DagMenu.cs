﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PXLCateringPCL.Model
{
    public class DagMenu
    {
        public string Title { get; set; }
        public DateTime Datum { get; set; }
        public string VoorGerecht { get; set; }
        public string HoofdGerecht { get; set; }
        public string NaGerecht { get; set; }
        public string Veggie { get; set; }
        public string Aanrader { get; set; }
        public string Suggestie { get; set; }
    }
}
