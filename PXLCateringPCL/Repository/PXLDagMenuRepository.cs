﻿using PXLCateringPCL.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json;

namespace PXLCateringPCL.Repository
{
    /// <summary>
    /// Retrieve DagMenu objects from api.pxl.be
    /// </summary>
    /// <remarks>
    /// based on: http://www.asp.net/web-api/overview/web-api-clients/calling-a-web-api-from-a-net-client
    /// In order to use HttpClient, you need to install nuget package (Microsoft.Net.Http)
    /// </remarks>
    public class PXLDagMenuRepository : IPXLDagMenuRepository
    {
        private string url = @"http://api.pxl.be/";
        private string guid = @"5fd1bc70-e4aa-438a-be97-50bbb9185da2";
        private string passwd = @"8e5`r:EY33rfHQ!4!+$*";

        private ObservableCollection<DagMenu> menulist;
        
        public async Task<ObservableCollection<DagMenu>> HuidigWeekMenu()
        {
            string offset = "Weekmenu";
            menulist = new ObservableCollection<DagMenu>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("applicationPassword", passwd);
                client.DefaultRequestHeaders.Add("applicationGuid", guid);

                HttpResponseMessage response = await client.GetAsync(offset);
                if (response.IsSuccessStatusCode)
                {
                    var result = await response.Content.ReadAsStringAsync();
                    result = Util.Markup.Remove(result);
                    var menus = JsonConvert.DeserializeObject<ObservableCollection<DagMenu>>(result);
                    menulist = menus;
                }
            }
            return menulist;
        }

        public async Task<DagMenu> HuidigDagMenu()
        {
            string offset = "dagmenu";
            DagMenu dagmenu = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("applicationPassword", passwd);
                client.DefaultRequestHeaders.Add("applicationGuid", guid);

                HttpResponseMessage response = await client.GetAsync(offset);
                if (response.IsSuccessStatusCode)
                {
                    var result = await response.Content.ReadAsStringAsync();
                    result = Util.Markup.Remove(result);
                    dagmenu = JsonConvert.DeserializeObject<DagMenu>(result);
                }
            }

            return dagmenu;
        }

        public string MaandagHuidigeWeek()
        {
            if ((menulist != null) && (menulist.Count() > 0))
            {
                return menulist.First().Datum.ToString("dd/MM/yyyy");
            }
            return "";
        }
    }
}
