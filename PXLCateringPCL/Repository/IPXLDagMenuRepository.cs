﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PXLCateringPCL.Model;
using System.Collections.ObjectModel;

namespace PXLCateringPCL.Repository
{
    public interface IPXLDagMenuRepository
    {
        Task<ObservableCollection<DagMenu>> HuidigWeekMenu();
        Task<DagMenu> HuidigDagMenu();
        string MaandagHuidigeWeek();
    }
}
