﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PXLCateringPCL.Util
{
    public class Markup
    {
        private const string HTML_TAG_PATTERN = "<.*?>|&#160;";
        private static Regex htmlRegex = new Regex(HTML_TAG_PATTERN);
        
        /// <summary>
        /// removes all markup from input string
        /// </summary>
        /// <param name="input">string containing markup</param>
        /// <returns>plain text not containing markup</returns>
        public static string Remove(string input)
        {
            return htmlRegex.Replace(input, String.Empty);
        }
    }
}
