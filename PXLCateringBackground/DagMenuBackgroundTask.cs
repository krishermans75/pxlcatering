﻿using NotificationsExtensions.TileContent;
using PXLCateringPCL.Model;
using PXLCateringPCL.Repository;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Background;
using Windows.UI.Notifications;

namespace PXLCateringBackground
{
    public sealed class DagMenuBackgroundTask : IBackgroundTask
    {
        public async void Run(IBackgroundTaskInstance taskInstance)
        {
            // Make sure that the background tasks doesn't terminate
            // before async operations are done.
            // Hence use a deferral.
            // source: http://msdn.microsoft.com/en-us/library/windows/apps/xaml/hh977055.aspx

            BackgroundTaskDeferral deferral = taskInstance.GetDeferral();
            Debug.WriteLine("Running DagMenuBackgroundTask");

            // Fetch daily menu
            IPXLDagMenuRepository repo = new PXLDagMenuRepository();
            DagMenu menu = await repo.HuidigDagMenu();

            // update Live Tile
            var tile = TileContentFactory.CreateTileWide310x150ImageAndText01();
            tile.TextCaptionWrap.Text = menu.HoofdGerecht;
            tile.Image.Src = "ms-appx:///Assets/pxl310x150.png";

            var square = TileContentFactory.CreateTileSquare150x150Text01();
            square.TextHeading.Text = "PCL Catering";
            square.TextBody1.Text = "Menu";
            tile.Square150x150Content = square;

            var xmlDocument = new Windows.Data.Xml.Dom.XmlDocument();
            xmlDocument.LoadXml(tile.GetContent()); 
            
            TileNotification notification = new TileNotification(xmlDocument);
            TileUpdateManager.CreateTileUpdaterForApplication().Update(notification);
            deferral.Complete();
        }
    }
}
